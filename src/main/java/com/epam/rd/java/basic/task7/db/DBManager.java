package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.*;

import com.epam.rd.java.basic.task7.db.entity.*;

public class DBManager {

    private static DBManager instance;
    private static Connection con;

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private DBManager() {
        con = getConnection();
    }

    private Connection getConnection() {
        Connection connection = null;
        try (FileInputStream fis = new FileInputStream("app.properties")) {
            Properties property = new Properties();
            property.load(fis);
            String url = property.getProperty("connection.url");
            connection = DriverManager.getConnection(url);
        } catch (Exception e) {
            System.out.println("Can't load app.properties");
        }

        return connection;
    }

    private void close(AutoCloseable ac) {
        if (ac != null) {
            try {
                ac.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void rollback(Connection con) {
        if (con != null) {
            try {
                con.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private static void setAutocommit() {
        try {
            con.setAutoCommit(true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<User> findAllUsers() throws DBException {
        Statement stmt = null;
        ResultSet rs = null;
        List<User> users = new ArrayList<>();
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery("select * from users");
            while (rs.next()) {
                users.add(extractUser(rs));
            }
        } catch (SQLException ex) {
            throw new DBException("Can't find all users: ", ex);
        } finally {
            close(rs);
            close(stmt);
        }

        return users;
    }

    public boolean insertUser(User user) throws DBException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            pstmt = con.prepareStatement("insert into users (login) values (?)", Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, user.getLogin());
            if (pstmt.executeUpdate() > 0) {
                rs = pstmt.getGeneratedKeys();
                if (rs.next()) {
                    user.setId(rs.getInt(1));
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            throw new DBException("Can't insert user: ", ex);
        } finally {
            close(rs);
            close(pstmt);
        }

        return true;
    }

    public boolean deleteUsers(User... users) throws DBException {
        PreparedStatement pstmt = null;
        try {
            con.setAutoCommit(false);
            pstmt = con.prepareStatement("delete from users where id=?");
            for (User user : users) {
                pstmt.setInt(1, user.getId());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            con.commit();
        } catch (SQLException ex) {
            rollback(con);
            throw new DBException("Can't delete users: ", ex);
        } finally {
            close(pstmt);
            setAutocommit();
        }

        return true;
    }

    public User getUser(String login) throws DBException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        User user = null;
        try {
            pstmt = con.prepareStatement("select * from users where login=?");
            pstmt.setString(1, login);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                user = extractUser(rs);
            }
        } catch (SQLException ex) {
            throw new DBException("Can't find user: ", ex);
        } finally {
            close(rs);
            close(pstmt);
        }

        return user;
    }

    public Team getTeam(String name) throws DBException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Team team = null;
        try {
            pstmt = con.prepareStatement("select * from teams where name=?");
            pstmt.setString(1, name);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                team = extractTeam(rs);
            }
        } catch (SQLException ex) {
            throw new DBException("Can't find team: ", ex);
        } finally {
            close(rs);
            close(pstmt);
        }

        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        Statement stmt = null;
        ResultSet rs = null;
        List<Team> teams = new ArrayList<>();
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery("select * from teams");
            while (rs.next()) {
                teams.add(extractTeam(rs));
            }
        } catch (SQLException ex) {
            throw new DBException("Can't find all teams: ", ex);
        } finally {
            close(rs);
            close(stmt);
        }

        return teams;
    }

    public boolean insertTeam(Team team) throws DBException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            pstmt = con.prepareStatement("insert into teams (name) values (?)", Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, team.getName());
            if (pstmt.executeUpdate() > 0) {
                rs = pstmt.getGeneratedKeys();
                if (rs.next()) {
                    team.setId(rs.getInt(1));
                }
            }
        } catch (SQLException ex) {
            throw new DBException("Can't insert team: ", ex);
        } finally {
            close(rs);
            close(pstmt);
        }

        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        PreparedStatement pstmt = null;
        try {
            con.setAutoCommit(false);
            pstmt = con.prepareStatement("insert into users_teams (user_id, team_id) values (?, ?)");
            for (Team team : teams) {
                pstmt.setInt(1, user.getId());
                pstmt.setInt(2, team.getId());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            con.commit();
        } catch (SQLException ex) {
            rollback(con);
            throw new DBException("Can't set teams for users: ", ex);
        } finally {
            close(pstmt);
            setAutocommit();
        }

        return true;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        final String sql = "" +
                "select t.* from teams t " +
                "join users_teams ut on t.id = ut.team_id " +
                "where ut.user_id=?";

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        List<Team> teams = new ArrayList<>();
        try {
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, user.getId());
            rs = pstmt.executeQuery();
            while (rs.next()) {
                teams.add(extractTeam(rs));
            }
        } catch (SQLException ex) {
            throw new DBException("Can't find user teams: ", ex);
        } finally {
            close(rs);
            close(pstmt);
        }

        return teams;
    }

    public boolean deleteTeam(Team team) throws DBException {
        PreparedStatement pstmt = null;
        try {
            pstmt = con.prepareStatement("delete from teams where id=?");
            pstmt.setInt(1, team.getId());
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            throw new DBException("Can't delete team: ", ex);
        } finally {
            close(pstmt);
        }

        return true;
    }

    public boolean updateTeam(Team team) throws DBException {
        PreparedStatement pstmt = null;
        try {
            pstmt = con.prepareStatement("UPDATE teams SET name = ? WHERE (id = ?)");
            pstmt.setString(1, team.getName());
            pstmt.setInt(2, team.getId());
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            throw new DBException("Can't update team: ", ex);
        } finally {
            close(pstmt);
        }

        return true;
    }

    public User extractUser(ResultSet rs) throws SQLException {
        User user = new User();
        user.setId(rs.getInt("id"));
        user.setLogin(rs.getString("login"));

        return user;
    }

    public Team extractTeam(ResultSet rs) throws SQLException {
        Team team = new Team();
        team.setId(rs.getInt("id"));
        team.setName(rs.getString("name"));

        return team;
    }

    public static void main(String[] args) {
        DBManager dbManager = new DBManager();
        try (Connection con = dbManager.getConnection()) {
            System.out.println(con);
        } catch (SQLException e) {
            System.out.println("" + e);
        }
    }

}
